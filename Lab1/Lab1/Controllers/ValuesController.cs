﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Lab1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        Mods.DBControl DBC = new Mods.DBControl();
        // GET api/values
        [HttpGet]
        public ActionResult<string> Get()
        {
            return "Добрый вечер!";
        }

        // GET api/values/*num*
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return DBC.GetByID(id);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
            DBC.PutInfo(value);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
