﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Lab1.Mods
{
    public class DBControl
    {
        public MySqlConnection MSConn = new MySqlConnection();
        public DBControl()
        {
            MySqlConnectionStringBuilder sb = new MySqlConnectionStringBuilder();
            sb.Server = Resource.SQLServerIP;
            sb.UserID = Resource.SQLUsername;
            sb.Database = Resource.SQLDBName;
            sb.Password = Resource.SQLPassword;
            string cs = sb.GetConnectionString(true);
            MSConn.ConnectionString = cs;
            MSConn.Open();
        }

        public string GetByID(int id)
        {
            string GString = "NULL";
            MySqlCommand GetComm = new MySqlCommand(@"SELECT INFO
                                                      FROM Lab1.L1
                                                      WHERE InfoID = " + id, MSConn);
            MySqlDataReader reader = GetComm.ExecuteReader();
            if(reader.HasRows)
            {
                reader.Read();
                GString = reader.GetString(0);
            }
            return GString;
        }

        public void PutInfo(string info)
        {
            int maxInt = getMaxInfoID();
            MySqlCommand PutComm = new MySqlCommand($"INSERT INTO Lab1.L1 VALUES({maxInt + 1}, {info})", MSConn);
            PutComm.ExecuteNonQuery();
        }

        private int getMaxInfoID()
        {
            int MaxID = 0;
            MySqlCommand GetComm = new MySqlCommand(@"SELECT MAX(InfoID)
                                                      FROM Lab1.L1");
            MySqlDataReader reader = GetComm.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                MaxID = reader.GetInt32(0);
            }
            return MaxID;
        }
    }
}
